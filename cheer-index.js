#! /usr/bin/env node
const { pickRandom, writeText } = require('./write.js')

const cheer = [
  "You are totally rad.",
  "Everyone talks about how awesome you are behind your back.",
  "You look amazing in a suit.",
  "You totally shred on that guitar.",
  "You are the sprinkles on the cupcake.",
  "You are both smart and handsome.",
  "You have endless talents.",
  "I like your style.",
  "On a scale from 1 to 10, you're an 11.",
  "I bet you sweat glitter.",
  "How is it that you always look great, even in sweatpants?",
  "Jokes are funnier when you tell them.",
  "You have cute elbows. For reals!",
  "You're more fun than bubble wrap.",
];

writeText(pickRandom(cheer));
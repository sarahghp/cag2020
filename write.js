const chalkAnimation = require('chalk-animation');

const animations = [
  'rainbow',
  'glitch',
  'neon',
  'karaoke',
];

const pickRandom = (arr) => {
  const idx = Math.floor(Math.random() * arr.length);
  return arr[idx];
}

const writeText = (text = "Twas brillig and the slithy toves") => {
  const affirm = chalkAnimation[pickRandom(animations)](text);
  setTimeout(() => {
    affirm.stop();
  }, 3000)
}

module.exports = {
  pickRandom,
  writeText,
}
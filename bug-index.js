#! /usr/bin/env node
const { pickRandom, writeText } = require('./write');

const bugs = [
"Don’t let that computer push you around.",
"You are smarter than the machine.",
"Go for a walk and it will come to you.",
"Squash it!",
"Have you considered throwing this laptop out the window?",
"Delete things!",
"Left channel, right channel, centre channel.",
"Imagine the piece as a set of disconnected events.",
"Repetition is a form of change.",
];

writeText(pickRandom(bugs));